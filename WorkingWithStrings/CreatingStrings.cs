﻿using System;

#pragma warning disable SA1611
#pragma warning disable SA1615

namespace WorkingWithStrings
{
    public static class CreatingStrings
    {
        public static string ReturnNewString(string str)
        {
            return new string(str);
        }

        public static string ReturnStringWithRepeatedChars(char c, int count)
        {
            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            return new string(c, count);
        }

        public static string ReturnStringFromCharArray(char[] c)
        {
            return new string(c);
        }

        /// <summary>
        /// Returns a new string that contains a part of a specified character array.
        /// </summary>
        public static string ReturnStringFromCharArray(char[] c, int startIndex, int length)
        {
            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (length < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length));
            }

            return new string(c, startIndex, length);
        }
    }
}
