﻿using System;
using System.Collections.Generic;

#pragma warning disable CA1062
#pragma warning disable SA1615
#pragma warning disable SA1611

namespace WorkingWithStrings
{
    public static class JoiningStrings
    {
        public static string GetCommaSeparatedString(string[] values)
        {
            return string.Join(',', values);
        }

        public static string GetColonSeparatedString(string[] values)
        {
            return string.Join(':', values);
        }

        public static string GetCommaSeparatedStringWithoutFirstElement(string[] values)
        {
            return string.Join(',', values, 1, values.Length - 1);
        }

        public static string GetHyphenSeparatedStringWithoutFirstAndLastElements(string[] values)
        {
            return string.Join('-', values, 1, values.Length - 2);
        }

        public static string GetPlusSeparatedString(IEnumerable<string> values)
        {
            return string.Join('+', values);
        }

        public static string GetBackslashSeparatedString(IEnumerable<object> values)
        {
            return string.Join('\\', values);
        }

        public static string GetStringSeparatedString(object[] values)
        {
            return string.Join("], [", values);
        }

        public static string GetStringSeparatedStringForLastThreeElements(string separator, string[] values)
        {
            return string.Join(separator, values, values.Length - 3, 3);
        }
    }
}
