﻿using System;

#pragma warning disable CA1062
#pragma warning disable SA1615
#pragma warning disable SA1611

namespace WorkingWithStrings
{
    public static class SplittingStrings
    {
        public static string[] SplitCommaSeparatedString(string str)
        {
            return str.Split(',');
        }

        public static string[] SplitColonSeparatedString(string str)
        {
            return str.Split(':');
        }

        public static string[] SplitCommaSeparatedStringMaxTwoElements(string str)
        {
            return str.Split(',', 2);
        }

        public static string[] SplitColonSeparatedStringMaxThreeElements(string str)
        {
            return str.Split(':', 3);
        }

        public static string[] SplitHyphenSeparatedStringMaxThreeElementsRemoveEmptyStrings(string str)
        {
            return str.Split('-', 3, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string[] SplitColonAndCommaSeparatedStringMaxFourElementsRemoveEmptyStrings(string str)
        {
            return str.Split(new char[] { ':', ',' }, 4, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string[] GetOnlyWords(string str)
        {
            return str.Split(new char[] { ' ', '.', ',', '!', '?', '»', '«', ':', '-', '\n', '\t' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string[] GetDataFromCsvLine(string str)
        {
            return str.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
