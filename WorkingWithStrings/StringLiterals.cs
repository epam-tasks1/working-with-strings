﻿using System;

#pragma warning disable SA1615

namespace WorkingWithStrings
{
    public static class StringLiterals
    {
        public static string ReturnAbcStringLiteral()
        {
            const string value = "abc";
            return value;
        }

        public static string ReturnAbcdefStringLiteral()
        {
            const string value = "ABCDEF";
            return value;
        }

        public static string ReturnEmptyString()
        {
            return string.Empty;
        }

        public static string ReturnFilePathStringLiteral()
        {
            const string value = "c:\\documents\\files\\myfile0234.txt";
            return value;
        }

        public static string ReturnFilePathVerbatimStringLiteral()
        {
            const string value = @"c:\documents\files\myfile0234.txt";
            return value;
        }
    }
}
